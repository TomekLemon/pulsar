<?php

try {
    require 'files/PHPMailerAutoload.php';
    $email = new PHPMailer();

    $bodytext = "<table>
                     <tr>
                        <th colspan='2'>Job Mail</th>
                     </tr>

                    <tr>
                        <td style='font-weight:bold'>Position:</td>
                        <td>" . $_POST['position'] . "</td>
                     </tr>

                     <tr>
                        <td style='font-weight:bold'>Name:</td>
                        <td>" . $_POST['name'] . "</td>
                     </tr>

                     <tr>
                      <td style='font-weight:bold'>E-mail: </td>
                      <td>" . $_POST['email'] . "</td>
                    </tr>
                 <table>";

    $bodytext = preg_replace('/\\\\/', '', $bodytext); //Strip backslashes

    $email->From = $_POST['email'];
    $email->FromName = "Job Mail";
    $email->isHTML(true);
    $email->addReplyTo($_POST['email'], $_POST['name']);
    $email->Subject = 'Job Mail';
    $email->Body = $bodytext;
    $email->AddAddress('beacons@pulsar.id');
    $email->CharSet = "UTF-8";
    $email->AddAttachment($_FILES['attachment']['tmp_name'], $_FILES['attachment']['name']);

//    $email->IsSMTP();                           // tell the class to use SMTP
//    $email->SMTPDebug  = 2;
//    $email->SMTPAuth   = true;                  // enable SMTP authentication
//    $email->Port       = 25;                    // set the SMTP server port
//    $email->Host       = "Lemonade.nazwa.pl"; // SMTP server
//    $email->Username   = "mailer@pulsar.id";     // SMTP server username
//    $email->Password   = "0rf{G&'.AxzS";            // SMTP server password

    $email->Send();
} catch (phpmailerException $e) {
    echo $e->errorMessage();
}