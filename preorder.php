<?php

try {
    require 'files/PHPMailerAutoload.php';
    $email = new PHPMailer();

    $bodytext = "<table>
                     <tr>
                        <th colspan='2'>Preorder Mail</th>
                     </tr>

                     <tr>
                        <td style='font-weight:bold'>First name:</td>
                        <td>" . $_POST['name'] . "</td>
                     </tr>

                     <tr>
                        <td style='font-weight:bold'>Last name:</td>
                        <td>" . $_POST['surname'] . "</td>
                     </tr>

                     <tr>
                      <td style='font-weight:bold'>E-mail: </td>
                      <td>" . $_POST['email'] . "</td>
                    </tr>

                     <tr>
                      <td style='font-weight:bold'>Country: </td>
                      <td>" . $_POST['country'] . "</td>
                    </tr>

                     <tr>
                      <td style='font-weight:bold'>Street Name: </td>
                      <td>" . $_POST['street'] . "</td>
                    </tr>

                     <tr>
                      <td style='font-weight:bold'>House Number: </td>
                      <td>" . $_POST['number'] . "</td>
                    </tr>

                     <tr>
                      <td style='font-weight:bold'>Postal Code: </td>
                      <td>" . $_POST['postal'] . "</td>
                    </tr>

                     <tr>
                      <td style='font-weight:bold'>Password: </td>
                      <td>" . $_POST['password'] . "</td>
                    </tr>
                 <table>";

    $bodytext = preg_replace('/\\\\/', '', $bodytext); //Strip backslashes

    $email->From = $_POST['email'];
    $email->FromName = "Preorder Mail";
    $email->isHTML(true);
    $email->addReplyTo($_POST['email'], $_POST['name']);
    $email->Subject = 'Preorder Mail';
    $email->Body = $bodytext;
    $email->AddAddress('beacons@pulsar.id');
    $email->CharSet = "UTF-8";

//    $email->IsSMTP();                           // tell the class to use SMTP
//    $email->SMTPDebug  = 2;
//    $email->SMTPAuth   = true;                  // enable SMTP authentication
//    $email->Port       = 25;                    // set the SMTP server port
//    $email->Host       = "Lemonade.nazwa.pl"; // SMTP server
//    $email->Username   = "mailer@pulsar.id";     // SMTP server username
//    $email->Password   = "0rf{G&'.AxzS";            // SMTP server password

    $email->Send();
} catch (phpmailerException $e) {
    echo $e->errorMessage();
}