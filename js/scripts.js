var force = false;
var jobOfferLiHeight = 64;
var jobOfferHeight = 62;
var fadeIn = 500;
var fadeOut = 0;

//$('.toAnimate').hide();

$('#fullpage').fullpage({
    'verticalCentered': true,
    'css3': false,
    'sectionsColor': ['#fff', '#aa134f', '#fff', '#fff', '#aa134f', '#fff'],
    'navigation': true,
    'navigationPosition': 'right',
    'navigationTooltips': ['Home', 'About', 'Science', 'Offer', 'Pre-order', 'Contact'],
    'scrollOverflow': true,
    'responsiveWidth': 979,
    'responsiveHeight': 649,
    'afterLoad': function (anchorLink, index) {

        if (index === 1) {
            $('#section_' + (index - 1)).find('.logo').addClass('animated').addClass('fadeInDown');
            $('#section_' + (index - 1)).find('h1').addClass('animated').addClass('fadeInUp');
            $('#section_' + (index - 1)).find('.teasers').addClass('animated').addClass('fadeIn');
            $('#section_' + (index - 1)).find('.pre-order').addClass('animated').addClass('fadeInLeft');
            $('#section_' + (index - 1)).find('.separator').addClass('animated').addClass('fadeIn');
            $('#section_' + (index - 1)).find('.learn-more').addClass('animated').addClass('fadeInRight');
        } else if (index === 2) {
            $('#section_' + (index - 1)).find('.left > div > div:nth-child(1)').addClass('animated').addClass('fadeInDown');
            $('#section_' + (index - 1)).find('.left > div > div:nth-child(2)').addClass('animated').addClass('fadeInUp');
            $('#section_' + (index - 1)).find('.right').addClass('animated').addClass('fadeInRight');
        } else if (index === 3) {
            $('#section_' + (index - 1)).find('.item').addClass('animated').addClass('fadeInUp');
        } else if (index === 4) {
            $('#section_' + (index - 1)).find('.item').addClass('animated').addClass('fadeInDown');
            $('#section_' + (index - 1)).find('.foot').addClass('animated').addClass('fadeIn');
        } else if (index === 5) {
            $('#section_' + (index - 1)).find('.item:nth-child(1)').addClass('animated').addClass('fadeInLeft');
            $('#section_' + (index - 1)).find('.separator').addClass('animated').addClass('fadeIn');
            $('#section_' + (index - 1)).find('.item:nth-child(3)').addClass('animated').addClass('fadeInRight');
            $('#section_' + (index - 1)).find('.foot').addClass('animated').addClass('fadeInUp');
            $('#section_' + (index - 1)).find('.top').addClass('animated').addClass('fadeIn');
        } else if (index === 6) {
            $('#section_' + (index - 1)).find('.item:nth-child(1)').addClass('animated').addClass('fadeInLeft');
            $('#section_' + (index - 1)).find('.item:nth-child(2)').addClass('animated').addClass('fadeInRight');
            $('#section_' + (index - 1)).find('.foot').addClass('animated').addClass('fadeInUp');
        }
        $('#section_' + (index - 1)).find('.container > h2').addClass('animated').addClass('fadeIn');
    },
    'onLeave': function (index, nextIndex, direction) {
        if (index == 5 && direction == 'down') {
            if ($(window).height() > 649 || $(window).width() > 979) {
                if ($('#preorder_now').is(':hidden') && !force) {
                    $.fn.fullpage.setAllowScrolling(false);
                    $('#preorder_now').stop(true, true).slideDown(500, function () {
                        $.fn.fullpage.setAllowScrolling(true);
                    });

                    return false;
                }
            }
        }

        if (index == 5 && direction == 'up') {
            if ($(window).height() > 649 || $(window).width() > 979) {
                if (!$('#preorder_now').is(':hidden') && !force) {
                    $.fn.fullpage.setAllowScrolling(false);
                    $('#preorder_now').stop(true, true).slideUp(500, function () {
                        $.fn.fullpage.setAllowScrolling(true);
                    });

                    return false;
                }
            }
        }

        if (nextIndex === 1) {
            $('#section_' + (nextIndex - 1)).find('.logo').css('opacity', '0').removeClass('animated').removeClass('fadeInDown');
            $('#section_' + (nextIndex - 1)).find('h1').css('opacity', '0').removeClass('animated').removeClass('fadeInUp');
            $('#section_' + (nextIndex - 1)).find('.teasers').css('opacity', '0').removeClass('animated').removeClass('fadeIn');
            $('#section_' + (nextIndex - 1)).find('.pre-order').css('opacity', '0').removeClass('animated').removeClass('fadeInLeft');
            $('#section_' + (nextIndex - 1)).find('.separator').css('opacity', '0').removeClass('animated').removeClass('fadeIn');
            $('#section_' + (nextIndex - 1)).find('.learn-more').css('opacity', '0').removeClass('animated').removeClass('fadeInRight');
        } else if (nextIndex === 2) {
            $('#section_' + (nextIndex - 1)).find('.left > div > div:nth-child(1)').css('opacity', '0').removeClass('animated').removeClass('fadeInDown');
            $('#section_' + (nextIndex - 1)).find('.left > div > div:nth-child(2)').css('opacity', '0').removeClass('animated').removeClass('fadeInUp');
            $('#section_' + (nextIndex - 1)).find('.right').css('opacity', '0').removeClass('animated').removeClass('fadeInRight');
        } else if (nextIndex === 3) {
            $('#section_' + (nextIndex - 1)).find('.item').css('opacity', '0').removeClass('animated').removeClass('fadeInUp');
        } else if (nextIndex === 4) {
            $('#section_' + (nextIndex - 1)).find('.item').css('opacity', '0').removeClass('animated').removeClass('fadeInDown');
            $('#section_' + (nextIndex - 1)).find('.foot').css('opacity', '0').removeClass('animated').removeClass('fadeIn');
        } else if (nextIndex === 5) {
            $('#section_' + (nextIndex - 1)).find('.item').css('opacity', '0').removeClass('animated').removeClass('fadeInLeft').removeClass('fadeInRight');
            $('#section_' + (nextIndex - 1)).find('.separator').css('opacity', '0').removeClass('animated').removeClass('fadeIn');
            $('#section_' + (nextIndex - 1)).find('.foot').css('opacity', '0').removeClass('animated').removeClass('fadeInUp');
            $('#section_' + (nextIndex - 1)).find('.top').css('opacity', '0').removeClass('animated').removeClass('fadeIn');
        } else if (nextIndex === 6) {
            $('#section_' + (nextIndex - 1)).find('.item').css('opacity', '0').removeClass('animated').removeClass('fadeInLeft').removeClass('fadeInRight');
            $('#section_' + (nextIndex - 1)).find('.foot').css('opacity', '0').removeClass('animated').removeClass('fadeInUp');
        }

        $('#section_' + (nextIndex - 1)).find('.container > h2').css('opacity', '0').removeClass('fadeIn');

        if (index == 6 && direction == 'up') {
            if ($(window).height() > 649 || $(window).width() > 979) {
                if (!force) {
                    $('#preorder_now').show();
                } else {
                    $('#preorder_now').hide();
                }
            }
        }

        if (index == 4 && direction == 'down') {
            if ($(window).height() > 649 || $(window).width() > 979) {
                if (!$('#preorder_now').is(':hidden')) {
                    $('#preorder_now').hide();
                }
            }
        }

        if (nextIndex !== 1) {
            if ($('#menu_top').is(':hidden')) {
                $('#menu_top').fadeIn(500);
            }
        } else {
            if (!$('#menu_top').is(':hidden')) {
                $('#menu_top').fadeOut(500);
            }
        }

        if (nextIndex === 6) {
            if ($('#footer').is(':hidden')) {
                $('#footer').fadeIn(500);
            }
        } else {
            if (!$('#footer').is(':hidden')) {
                $('#footer').fadeOut(500);
            }
        }

        force = false;
        setActive(nextIndex);

        resize();
    },
    'afterResize': function () {
        resize();
    }
});

$('#menu_top li a, .logo, .learn-more').on('click', function (e) {
    e.preventDefault();

    var index = 0;

    if (!$(this).hasClass('logo') && !$(this).hasClass('learn-more')) {
        index = $(this).parent('li').index();
    } else if ($(this).hasClass('learn-more')) {
        index = 1;
    }


    var element = $('#fp-nav ul li a').get(index);
    $(element).trigger('click');
});

$('#fp-nav a').on('click', function () {
    force = true;
});

$('#section_1 .left, #section_1 .right').each(function () {
    $(this).find('> div').height($(this).parents('.container').height());
});

$('.job-offers > a').on('click', function () {
    var windowHeight = $(this).parents('.section').height();
    var posibleWindowHeight = ($(this).parents('.job-offers').find('ul li').length * jobOfferLiHeight) + $(this).offset().top + jobOfferHeight;

    if (windowHeight < posibleWindowHeight) {
        $(this).parents('.job-offers').find('ul').css('top', 'auto');
        $(this).parents('.job-offers').find('ul').css('bottom', '60px');
    } else {
        $(this).parents('.job-offers').find('ul').css('bottom', 'auto');
        $(this).parents('.job-offers').find('ul').css('top', '60px');
    }

    if ($(this).parents('.job-offers').find('ul').is(':hidden')) {
        $(this).parents('.job-offers').find('ul').slideDown(500);
    } else {
        $(this).parents('.job-offers').find('ul').slideUp(500);
    }
});

$('.section').superslides({
    //'inherit_height_from': '.section'
});
resize();

function resize() {
    $('.job-offers').find('ul').hide();

    if ($(window).height() > 649 && $(window).width() > 979) {
        $('#footer').css('position', 'fixed');
        $('#footer').css('bottom', '0px');
        fadeOut = fadeIn;
    } else {
        $('#footer').css('position', 'absolute');
        $('#footer').css('position', 'absolute');
        $('#footer').css('bottom', '-90px');
        //$('.popup').css('max-height', ($(window).height() - 50) + 'px');
        fadeOut = 0;
    }
}

function setActive(index) {
    if (index) {
        $('#menu_top li a').removeClass('active');

        var element = $('#menu_top li').get(index - 1);
        $(element).find('a').addClass('active');
    }
}

$('.checkbox label').prepend('<span></span>');

$('.popup form > div input[type=text], .popup form > div input[type=password], .popup form > div input[type=email], .popup form > div textarea').on('change', function () {
    checkInput(this);
});

$('.popup form > div input[type=text], .popup form > div input[type=password], .popup form > div input[type=email], .popup form > div textarea').on('focus', function () {
    $(this).prev('label').addClass('animated').addClass('slideInUp');
});

$('.popup form > div input[type=text], .popup form > div input[type=password], .popup form > div input[type=email], .popup form > div textarea').on('blur', function () {
    $(this).prev('label').removeClass('animated').removeClass('slideInUp');
});

$('.popup form > div input[type=text], .popup form > div input[type=password], .popup form > div input[type=email], .popup form > div textarea').each(function () {
    checkInput(this);
});

function checkInput(that) {
    $(that).val($.trim($(that).val()));

    if ($(that).val() === "") {
        $(that).prev('label').removeClass('filled');
    } else {
        $(that).prev('label').addClass('filled');
    }
}

$('.overlay').on('click', function () {
    $(this).find('.popup').fadeOut(fadeOut);
    $(this).fadeOut(fadeOut);

    $.fn.fullpage.setAllowScrolling(true);
});


function validatePwdValid(e, pwdValid) {

    if (!pwdValid) {
        e.preventDefault();
    }
}

$("form").on('submit', function (e) {
    validatePwdValid(e, $(this).valid());
});

$('.popup').on('click', function (e) {
    e.stopPropagation();
});

$('.pre-order').on('click', function (e) {
    e.preventDefault();

    $('.preorder').fadeIn(fadeIn);
    $('.overlay').css('display', 'table');

    $.fn.fullpage.setAllowScrolling(false);
});

$('.become-partner').on('click', function (e) {
    e.preventDefault();

    $('.partner').fadeIn(fadeIn);
    $('.overlay').css('display', 'table');

    $.fn.fullpage.setAllowScrolling(false);
});

$('.job-offers ul li > a').on('click', function (e) {
    e.preventDefault();

    $('.job').fadeIn(fadeIn);
    $('.overlay').css('display', 'table');

    $('.job').find('#jobPosition').val($(this).html());
    $('.job').find('.title').html($(this).html());

    $.fn.fullpage.setAllowScrolling(false);
});

$('#jobForm, #partnerForm, #preorderForm').ajaxForm(function () {
    alert("Your mail has been send!");

    $('.overlay').find('.popup').fadeOut(fadeOut);
    $('.overlay').fadeOut(fadeOut);

    $.fn.fullpage.setAllowScrolling(true);
});

$('.attachment').on('click', function () {
    $('#uploadBtn').trigger('click');
});

$('#uploadBtn').on('change', function () {
    $('.attachment').html($(this).val());
});

$("#jobForm").validate({
    rules: {
        name: "required",
        email: {
            email: true
        },
        attachment: "required"
    },
    messages: {
        name: "Please enter your firstname",
        email: "Please enter a valid email address",
        attachment: "This field is required"
    }
});

$("#preorderForm").validate({
    rules: {
        name: "required",
        surname: "required",
        country: "required",
        street: "required",
        number: "required",
        postal: "required",
        password: "required",
        agreement: "required",
        email: {
            email: true
        }
    },
    messages: {
        name: "This field is required",
        surname: "This field is required",
        country: "This field is required",
        street: "This field is required",
        number: "This field is required",
        postal: "This field is required",
        password: "This field is required",
        email: "Please enter a valid email address",
        agreement: "Please accept our policy"
    }
});

$("#partnerForm").validate({
    rules: {
        name: "required",
        about: "required",
        email: {
            email: true
        }
    },
    messages: {
        name: "This field is required",
        surname: "This field is required",
        about: "This field is required"
    }
});